const { error } = require('console')
const { request, response } = require('express')
const express=require('express')
const pool = require('./db')


const db=require ('./db')
const utils=require('./utils')
const router=express.Router()

router.get('/get',(request,response) => {
    const statement =`select * from book`;
    db.execute(statement,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
    

})

router.post('/add',(request,response) => {
    const {id,  title, name, author } =request.body

    const statement =`insert into book (book_id, book_title, publisher_name, author_name) 
    values('${id}','${title}','${name}','${author}')`;

    db.execute(statement,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
    

})
router.put('/update',(request,response) => {
    const {id, name, author } =request.body

    const statement =`update book set publisher_name='${name}', author_name='${author}' 
    where book_id='${id}'`;
    
    db.execute(statement,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
    

})
router.delete('/delete/:id',(request,response) => {
    const {id} =request.params
    const statement =`delete from book 
    where book_id='${id}'`;
    
    db.execute(statement,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
    

})



module.exports = router
