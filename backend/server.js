const express=require('express')
const app = express()
const routerbook=require('./book')
const cors=require('cors')


app.use(express.json())
app.use(routerbook)
app.use(cors('*'))


app.listen(4000,()=> {
console.log("server started on port 4000")
})
